<?php

return [
    'settings' => [
        'general' => 'OmniPaySagePay',
        'tab' => 'OmniPay',
        'vendor' => 'Vendor',
        'testmode' => 'Test Mode',
    ],
    'operators' => [
        'omnipay_sagepay' => 'SagePay',
    ],
];