<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 11/18/15
 * Time: 10:39 PM
 */

namespace Keios\PGOmniPaySagePay\Operators;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use Keios\PaymentGateway\Core\Operator;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use October\Rain\Exception\ValidationException;
use Omnipay\Common\CreditCard;
use Omnipay\Common\Message\ResponseInterface;
use Omnipay\Omnipay;
use Omnipay\SagePay\Message\Response;
use Omnipay\SagePay\Message\ServerCompleteAuthorizeResponse;
use Omnipay\SagePay\ServerGateway;

class OmniPaySagePay extends Operator
{
    use SettingsDependent;

    /**
     * For future use
     */
    const SAGEPAY_MODE = 'SagePay_Direct';

    /**
     * To override, checked on purchase
     */
    const CREDIT_CARD_REQUIRED = true;

    /**
     * To override, defines if extended card partial should be used
     */
    const CREDIT_CARD_BILLING_ADDRESS_REQUIRED = true;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgomnipaysagepay::lang.operators.omnipay_sagepay';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgomnipaysagepay/assets/img/logo/sagepay.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];

    /**
     * @var ServerGateway
     */
    protected $driver;

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendPurchaseRequest()
    {
        $this->makeOmniPayDriver();

        /**
         * @var Response $response
         */
        $response = $this->driver->purchase($this->generateGatewayOptions())->send();

        $this->transactionRef = $response->getTransactionReference();

        if ($response->isSuccessful()) {

            // last argument marks response as final, lack of errors array marks as successful
            return new PaymentResponse($this, null, null, null, true);

        } elseif ($response->isRedirect()) {

            return new PaymentResponse($this, $response->getRedirectUrl(), null, $response->getRedirectData());

        } else {

            return new PaymentResponse($this, null, [$response->getMessage()]);
        }
    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data)
    {
        if ($this->can(self::TRANSITION_ACCEPT) && $this->transactionRef) {
            $transRef = $this->transactionRef;
        } else {
            $transRef = null;
        }

        $this->makeOmniPayDriver();

        $requestMessage = $this->driver->completePurchase(
            [
                'transactionId'        => $this->uuid,
                'transactionReference' => $transRef,
            ]
        );

        try {
            $responseMessage = $requestMessage->send();
        } catch (\Exception $e) {
            // hack from https://github.com/academe/OmniPay-SagePay-Demo/blob/master/sagepay-confirm.php
            $requestMessage = $this->driver->completePurchase([]);
            $responseMessage = new ServerCompleteAuthorizeResponse($requestMessage, []);
            $responseMessage->invalid($this->returnUrl, $e->getMessage());
        }

        if ($responseMessage->isSuccessful()) {
            $this->accept();
        } else {
            $this->rejectFromApi();
        }

        $responseMessage->confirm($this->returnUrl);
    }

    /**
     * @throws ValidationException
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {
        throw new ValidationException(['error' => 'Refunds are not yet available for SagePay.']);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['VendorTxCode'])) {
            return $data['VendorTxCode'];
        } elseif (isset($data['pgUuid'])) {
            return $data['pgUuid'];
        } else {
            return 'ERROR';
        }
    }

    /**
     * @return array
     */
    protected function generateGatewayOptions()
    {
        $ccOptions = [
            'firstName'         => $this->creditCard->getFirstName(),
            'lastName'          => $this->creditCard->getLastName(),
            'number'            => $this->creditCard->getNumber(),
            'expiryMonth'       => $this->creditCard->getExpiryMonth(),
            'expiryYear'        => $this->creditCard->getExpiryYear(),
            'cvv'               => $this->creditCard->getCvv(),
            'email'             => $this->creditCard->getEmail(),
            'type'              => $this->creditCard->getBrand(),
            'billingFirstName'  => $this->creditCard->getFirstName(),
            'billingLastName'   => $this->creditCard->getLastName(),
            //            'billingCompany'    => $this->creditCard, // todo add support for business cards
            'billingAddress1'   => $this->creditCard->getBillingAddress(),
            //            'billingAddress2'   => $this->creditCard,
            'billingCity'       => $this->creditCard->getBillingCity(),
            'billingPostcode'   => $this->creditCard->getBillingZip(),
            'billingCountry'    => $this->creditCard->getBillingCountry(),
            'billingState'      => $this->creditCard->getBillingState(),
            'shippingFirstName' => $this->creditCard->getFirstName(), // todo
            'shippingLastName'  => $this->creditCard->getLastName(), // todo
            //                        'shippingCompany'   => $this->creditCard, // todo
            'shippingAddress1'  => $this->creditCard->getBillingAddress(), // todo
            //                        'shippingAddress2'  => $this->creditCard, // todo
            'shippingCity'      => $this->creditCard->getBillingCity(), // todo
            'shippingPostcode'  => $this->creditCard->getBillingZip(), // todo
            //                        'shippingState'     => $this->creditCard, // todo
            'shippingCountry'   => $this->creditCard->getBillingCountry(), // todo
            'shippingState'     => $this->creditCard->getBillingState(),
            //                        'shippingPhone'     => $this->creditCard, // todo
            //                        'shippingFax'       => $this->creditCard, // todo
        ];
        $this->validateState($ccOptions);

        $cc = new CreditCard($ccOptions);

        return [
            'card'          => $cc,
            'amount'        => $this->cart->getTotalGrossCost()->getAmountBasic(),
            'currency'      => $this->cart->getTotalGrossCost()->getCurrency()->getIsoCode(),
            'description'   => $this->paymentDetails->getDescription(),
            'transactionId' => $this->uuid,
            'clientIp'      => Request::getClientIp(),
            'returnUrl'     => URL::to('_paymentgateway/'.OperatorUrlizer::urlize($this)).'?pgUuid='.$this->uuid,
            //'cancelUrl'     => '', // not supported
        ];
    }

    protected function validateState($array)
    {
        if ($array['billingCountry'] == 'US') {
            $rules = [
                'billingState' => 'required',
            ];
            $messages = [
                'required' => 'Please select state',
            ];

            $validator = \Validator::make($this->creditCard->getParameters(), $rules, $messages);

            if ($validator->fails()) {
                throw new \ValidationException($validator);
            }
        }
    }

    /**
     * @return null
     */
    protected function makeOmniPayDriver()
    {
        $this->driver = Omnipay::create(self::SAGEPAY_MODE);

        $settings = $this->getSettings();

        $this->driver->setVendor($settings->get('omnipaysagepay.vendor'));
        $this->driver->setTestMode((boolean)$settings->get('omnipaysagepay.testmode'));
    }
}