<?php namespace Keios\PGOmniPaySagePay;

use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;

use Keios\PGOmniPaySagePay\Operators\OmniPaySagePay;
use System\Classes\PluginBase;

/**
 * PGOmniPayBase Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PGOmniPayBase',
            'description' => 'Connects OmniPay library to PaymentGateway',
            'author'      => 'Keios',
            'icon'        => 'icon-dollar',
        ];
    }

    public function register()
    {
        $this->app['events']->listen(
            'paymentgateway.booted',
            function () {

                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', OmniPaySagePay::class);
            }
        );

        $this->app['events']->listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'omnipaysagepay.general' => [
                            'label' => 'keios.pgomnipaysagepay::lang.settings.general',
                            'tab'   => 'keios.pgomnipaysagepay::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'omnipaysagepay.info'    => [
                            'type' => 'partial',
                            'path' => '$/keios/pgomnipaysagepay/partials/_omnipaysagepay_info.htm',
                            'tab'  => 'keios.pgomnipaysagepay::lang.settings.tab',
                        ],
                        'omnipaysagepay.vendor'   => [
                            'label'   => 'keios.pgomnipaysagepay::lang.settings.vendor',
                            'tab'     => 'keios.pgomnipaysagepay::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'omnipaysagepay.testmode' => [
                            'label'   => 'keios.pgomnipaysagepay::lang.settings.testmode',
                            'tab'     => 'keios.pgomnipaysagepay::lang.settings.tab',
                            'type'    => 'dropdown',
                            'options' => [
                                true => 'Testing',
                                false => 'Live'
                            ],
                        ]
                    ]
                );
            }
        );
    }

}
